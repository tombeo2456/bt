<?php

use yii\db\Migration;

/**
 * Class m190417_143809_my_order
 */
class m190417_143809_my_order extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%my_order}}', [
            'id' => $this->primaryKey(),
            'product_name' => $this->string()->notNull(),
            'price' => $this->decimal(15,2)->notNull()->defaultValue(0),
            'user_id' => $this->integer()->notNull(),
            'purchase_date'   => $this->date()->notNull(),
        ], $tableOptions);
        // Tạo chỉ mục cho cột
        $this->createIndex(
            'idx-my_order-product_name',// Đăt tên khóa
            'my_order',
            'product_name'
        );
        $this->addForeignKey(
            'fk-post-product_name', // Tên khóa
            'my_order',             // Bảng chứa khóa phụ:@my_order
            'product_name',         // Khóa phụ: @product_name
            'items',                // Bảng lấy khóa phụ:@items
            'item_name',            // Khóa chính @item_name
            'CASCADE'
        );
    }

    public function down()
    {
        $this->dropTable('{{%my_order}}');
    }
}
