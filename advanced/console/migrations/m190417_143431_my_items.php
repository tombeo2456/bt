<?php

use yii\db\Migration;

/**
 * Class m190417_143431_my_items
 */
class m190417_143431_my_items extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%items}}', [
            'item_name' => $this->string()->notNull(),
            'item_producer' => $this->string()->notNull(),
            'item_expiry_date' => $this->date()->notNull(),
            'PRIMARY KEY(item_name)',  //Tạo khóa chính phải tạo như thế này
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%items}}');
    }
}
