<?php

use yii\db\Migration;

/**
 * Class m190410_163258_category
 */
class m190410_163258_category extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%category}}', [
            'id' => $this->primaryKey(),  //đây là khóa chính
            'name' => $this->string()->notNull()->unique(),
            'slug' => $this->string()->notNull()->unique(),  //slug: Tên đường dẫn lưu dạng tên mà mình cần đến
            'parent' => $this->integer()->notNull()->defaultValue(0), //parent là cha và mặc định là defaultValue(0)
            'status' => $this->smallInteger()->notNull()->defaultValue(0),// trường này là có được kích hoạt hay không
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%category}}');
    }
}
