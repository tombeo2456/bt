<?php

use yii\db\Migration;

/**
 * Class m190411_074955_contact
 */
class m190411_074955_contact extends Migration
{
    public function up()
    {
     $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%contact}}', [
            'id' => $this->primaryKey(),  //đây là khóa chính
            'name' => $this->string()->notNull(), //string(): không ghi gì trong string là max luôn string(255)
            'email' => $this->string(100)->notNull(),
            'subject' => $this->string()->notNull(),  //slug: Tên đường dẫn lưu dạng tên mà mình cần đến    //Bên contact thì có thể contact nhiều lần nên không cần unique(). Unique(): là để không cho dữ liệu trùng lặp
            'body' => $this->text()->notNull(),
            'parent' => $this->integer()->notNull()->defaultValue(0), //parent là cha và mặc định là defaultValue(0)
            'status' => $this->smallInteger()->notNull()->defaultValue(0),// trường này là có được kích hoạt hay không
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%contact}}');
    }
}
