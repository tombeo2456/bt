<?php 
 namespace frontend\controllers;  //Là thư mục chứa của nó
 
 use yii\web\Controller;

 class DemoController extends Controller
 {
     /**
      * summary
      */
     public function actionIndex()
     {
         //echo 'Welcome to yi';
     	return $this->render('index');
     }

     public function actionHello()
     {
     	return $this->render('hello');
     }

     public function actionHelloWorld()
     {
         return $this->render('hello-world');
     }
 }
 
 ?>