<?php 
	namespace frontend\controllers;  //đây là chỗ chứa file đang (file đang ở đâu thì namespace đến đó)
	use yii\web\Controller;
	

	class TinTucController extends Controller
	{
	    /**
	     * summary
	     */
	    public function actionIndex()
	    {
	        return $this->render('index');
	    }

	    public function actionTinTheGioi()
	    {	
	    	return $this->render('tin-the-gioi');
	    }
	}
	
 ?>