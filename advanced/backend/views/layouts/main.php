<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>  <!-- Đây là nơi chứa, nó tự động then các code css --> 
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap bg-info">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-static-top',   
            // 'class' => 'navbar-inverse navbar-fixed-top',   //cái fixed: Là cái che màn hình của bootstap.. Cái này là sẵn có của yii
        ],
    ]);
    $menuItems = [
        ['label' => 'Home', 'url' => ['/site/index']],
        ['label' => 'MyOrder', 'url' => ['/my_order/my-order/index']],
        ['label' => 'MyItems', 'url' => ['/my_order/my-items/index']],
    ];
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
    } else {
        $menuItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Logout (' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn btn-link logout',]
            )
            . Html::endForm()
            . '</li>';
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2 aside-left bg-info">
                    <div class="card">
                        <div class="card-block">
                            <p class="card-text">
                                        <ul class="list-group">

                                            <li class="list-group-item">
                                            <?php echo Html::a('<span class="glyphicon glyphicon-home"></span> Trang chủ', ['/site']); ?>  <!--  truyền Html-> thẻ a là nó sẽ tạo ra 1 cái thẻ a như ở bên dưới -->   <!-- Cái đầu tiên truyền vào là cái tiêu đề -->   <!-- Cái thứ 2 truyền vào là cái link controller->index(acction đầu tiên... Có thể để rỗng) -->    <!-- Cái thứ 3 có hay hoặc không có đều được -->
                                                <!-- <a href="" title="">Quản lý danh mục</a> -->
                                            </li>

                                            <li class="list-group-item">
                                            <?php echo Html::a('<span class="glyphicon glyphicon-th-list
                                            "></span> Quản lý danh mục', ['/category']); ?>  <!--  truyền Html-> thẻ a là nó sẽ tạo ra 1 cái thẻ a như ở bên dưới -->   <!-- Cái đầu tiên truyền vào là cái tiêu đề -->   <!-- Cái thứ 2 truyền vào là cái link controller->index(acction đầu tiên... Có thể để rỗng) -->    <!-- Cái thứ 3 có hay hoặc không có đều được ['class' => 'btn btn-link'] -->
                                                <!-- <a href="" title="">Quản lý danh mục</a> -->
                                            </li>

                                            <li class="list-group-item">
                                            <?php echo Html::a('<span class="glyphicon glyphicon-th-list
                                            "></span> Quản lý My Order', ['/my_order/my-order']); ?>  <!--  truyền Html-> thẻ a là nó sẽ tạo ra 1 cái thẻ a như ở bên dưới -->   <!-- Cái đầu tiên truyền vào là cái tiêu đề -->   <!-- Cái thứ 2 truyền vào là cái link controller->index(acction đầu tiên... Có thể để rỗng) -->    <!-- Cái thứ 3 có hay hoặc không có đều được ['class' => 'btn btn-link'] -->
                                                <!-- <a href="" title="">Quản lý danh mục</a> -->
                                            </li>

                                            <li class="list-group-item">
                                            <?php echo Html::a('<span class="glyphicon glyphicon-th-list
                                            "></span> Quản lý My Items', ['/my_order/my-items']); ?>  <!--  truyền Html-> thẻ a là nó sẽ tạo ra 1 cái thẻ a như ở bên dưới -->   <!-- Cái đầu tiên truyền vào là cái tiêu đề -->   <!-- Cái thứ 2 truyền vào là cái link controller->index(acction đầu tiên... Có thể để rỗng) -->    <!-- Cái thứ 3 có hay hoặc không có đều được ['class' => 'btn btn-link'] -->
                                                <!-- <a href="" title="">Quản lý danh mục</a> -->
                                            </li>
                                        </ul>
                            </p>
                        </div>
                    </div>
            </div>
        <div class="col-md-10 admin-right">
            <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= Alert::widget() ?>
            <?= $content ?>  <!-- Biến content này lấy theo các cái view tương ứng -->
        </div>
        </div>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; <?= Html::encode(Yii::$app->name) ?> <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
