<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Democate */

$this->title = 'Sửa: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Danh mục', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="democate-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
