<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Democate */

$this->title ='View: ' .$model->name;
$this->params['breadcrumbs'][] = ['label' => 'Democates', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="democate-view">

            <div class="card">
                <div class="card-block">
                <h4 class="card-title"><?= Html::encode($this->title) ?></h4>
                <p class="card-text">
        <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        </p>

        <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
        'id',
        'name',
        'slug',
        'parent',
        'status',
        'created_at',
        'updated_at',
        ],
    ]) ?>
                    </p>
                    <a href="#" class="btn btn-primary">Button</a>
                </div>
            </div>

</div>
