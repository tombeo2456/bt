<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Democate */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="democate-form">

    		<div class="card">
    			<div class="card-block">
    				<h4 class="card-title"><?= Html::encode($this->title) ?></h4>
    				<p class="card-text">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'parent')->textInput() ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
	</p>
	

</div>
</div>

</div>
