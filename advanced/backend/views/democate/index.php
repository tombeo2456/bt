<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\Democate;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\DemocateSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Danh mục';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="democate-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <div class="card">
                <div class="card-block">
                    <h4 class="card-title"><?= Html::encode($this->title) ?></h4>
                    <p class="card-text">
        <p class="pull-right">
        <?= Html::a('Thêm mới', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
        <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
        //     ['class' => 'yii\grid\SerialColumn',
        //     'header' => 'STT',
        //     'headerOptions' => [
        //         'style' => 'width:15px; text-align:center'
        //     ],
        //     'contentOptions' => [
        //         'style' => 'width:15px; text-align:center'
        //     ],
        // ],

        [
            'class' =>'yii\grid\checkboxColumn'
        ],

        [
            'attribute' => 'id',
            'label' => 'ID',
            'headerOptions' => ['style' => 'width:15px; text-align:center'],
            'contentOptions' => ['style' => 'width:15px; text-align:center'],
        ],

            'name',
            'slug',
            // 'parent',
            [
                'attribute' => 'parent',
                'content' => function($model)
                {
                    if ($model->parent==0) {
                        return 'Root ';
                    } else {
                       $parent = Democate::find()->where(['id' => $model-> parent])->one();
                       if ($parent) {
                           return $parent->name;
                       } else {
                            return 'không có';                           
                       }
                    }
                },
                'headerOptions' => ['style' =>'width:150px; text-align:center'],
                'contentOptions' => ['style' => 'width:150px; text-align:center'],
            ],


            // 'status',
            [
                'attribute' => 'status',
                'content' =>function($model)
                {
                    if ($model->status==0) {
                        return '<span class="label label-danger">Không kích hoạt</span>';
                    } else {
                       return '<span class="label label-success">Kích hoạt</span>' ;
                    }
                },
                'headerOptions' => ['style' => 'width:150px; text-align:center'],
                'contentOptions' => ['style' => 'width:150px; text-align:center'],
            ],

            // 'created_at',
            [
                'attribute' => 'created_at',
                'content' => function($model)
                {
                    return date('d-m-Y', $model->created_at);
                },
                'headerOptions' => ['style' => 'width:150px; text-align:center'],
                'contentOptions' => ['style' => 'width:150px; text-align:center'],
            ],

            //'updated_at',

            ['class' => 'yii\grid\ActionColumn',
              'buttons' => [
                'view' => function($url, $model)
                {
                    return Html::a('View', $url, ['class' => 'btn btn-sm btn-primary']);
                },
                'update' => function($url, $model)
                {
                    return Html::a('Update', $url, ['class' => 'btn btn-sm btn-success']);
                },
                'delete' => function($url, $model)
                {
                    return Html::a('Delete', $url, [
                        'class' => 'btn btn-sm btn-danger',
                        'data-confirm' => 'Bạn có chắc chắn xóa ' .$model->name,
                        'data-method' => 'post '
                    ]);
                },
              ],
            ],
        ],
    ]); ?>


</div>
