<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Democate */

$this->title = 'Thêm mới danh mục ';
$this->params['breadcrumbs'][] = ['label' => 'Danh  mục', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="democate-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
