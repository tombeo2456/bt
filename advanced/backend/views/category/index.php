<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\Category;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Danh mục';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="panel panel-primary">
        <div class="panel-heading"><?= Html::encode($this->title) ?></div>  <!-- Phải có phần này thì mới chạy đc panel -->
        <div class="panel-body">
    <p class="pull-right">
        <?= Html::a('Thêm mới', ['create'], ['class' => 'btn btn-success btn-sm']) ?>
    </p>
            <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,

        //đây chính là cái dấu thăng ở đầu tiên khi ta lập 
        //Khi dùng check box thì có thể bỏ cái này đi được không cần hiện STT ra
        'columns' => [
        //     ['class' => 'yii\grid\SerialColumn',
        //     'header' => 'STT',
        //     'headerOptions' => ['style' => 'width:15px;text-align:center'],

        //      //Phần nội dung thì nó gọi là cái contents
        //     'contentOptions' => ['style' => 'width:15px;text-align:center'],
        // ],

        //Check box column
        ['class' => 'yii\grid\CheckBoxColumn'],

        //khai báo attribute
        [
            'attribute' => 'id',  //attribute: là để hiện số thứ tự của từng id được nhập vào 1..  //attribute: làm cho các phần mình muốn bé lại không chiếm phần quá to
            'label' => 'ID', //Để là label thì sẽ có thể sắp xếp còn để header thì không sắp xếp đc
            'headerOptions' => ['style' => 'width:15px;text-align:center'],

             //Phần nội dung thì nó gọi là cái contents
            'contentOptions' => ['style' => 'width:15px;text-align:center'],
        ],

        //Trạng thái thì cũng giống như trên. Có 2 trạng thái đơn giản là kích hoạt hoặc không kích hoạt


            //Khai báo các dòng theo hàng ngang ở đầu tiền của bảng
            'name',
            'slug',
            // 'parent',
             [
                'attribute' => 'parent',
                //Muốn cái trạng thái 1: kích hoạt.  0: không kích hoạt thì là content => truyền vào 1 biến function($model)
                'content' => function($model){
                    if ($model->parent==0) {
                        return 'Root ';
                    } else {
                         $parent = Category::find()->where(['id'=> $model-> parent])->one();       //where: Là hiện tại của nó =0 thì nó là Root.   Còn parent #0 thì phải tìm cha của nó, cái parent này =id nào đó.    1 danh mục con thì chỉ có thể có 1 cha.   Còn 1 cha có thể có nhiều thư mục con
                         if ($parent) {
                             return $parent->name;
                         } else {
                             return 'Không rõ';
                         }
                         return '<span class="label label-success">Kích hoạt</span>';
                    }
                },
                // 'label' => '',    //Label: Nếu chúng ta không đặt thì nó tự động lấy trong model
                'headerOptions' => ['style' => 'width:150px; text-align:center'], //width: là độ rộng
                'contentOptions' => ['style' => 'width:150px; text-align:center'],
            ],

            // 'status',
            [
                'attribute' => 'status',
                //Muốn cái trạng thái 1: kích hoạt.  0: không kích hoạt thì là content => truyền vào 1 biến function($model)
                'content' => function($model){
                    if ($model->status==0) {
                        return '<span class="label label-danger">Không kích hoạt</span>';
                    } else {
                         return '<span class="label label-success">Kích hoạt</span>';
                    }
                },
                // 'label' => '',    //Label: Nếu chúng ta không đặt thì nó tự động lấy trong model
                'headerOptions' => ['style' => 'width:150px; text-align:center'], //width: là độ rộng
                'contentOptions' => ['style' => 'width:150px; text-align:center'],
            ],

            //'created_at',
            [
                'attribute' =>'created_at',
                //'label' =>  //Tự sinh ra
                'content' => function($model){
                    return date('d-m-Y', $model->created_at);  //Bt truyền vào 1 or 2 tham số.   //Tham số đầu là  format    //Tham số thứ 2 là thời gian

                }
            ],


            //'updated_at',

            ['class' => 'yii\grid\ActionColumn',     //cái action là 3 cái nút ở cuối cùng
                'buttons' => [
                    'view' => function($url, $model){  //url: để nó trỏ đến cái url của mình
                        return Html::a('View', $url, ['class' =>'btn btn-xs btn-primary']) ;    //Html ->(trỏ đến) a -> truyền vào 1 cái label(VD: View), t2: là đường dẫn url, t3: là class(VD: btn btn-sm )
                    },

                    'update' =>function($url, $model){
                        return Html::a('Update', $url, ['class' => 'btn btn-xs btn-success']);
                    },

                    'delete' =>function($url, $model){
                        return Html::a('<span class="glyphicon glyphicon-remove"</span>Delete', $url,  ['class' => 'btn btn-xs btn-danger',
                                                         'data-confirm' => 'Bạn có chắc chắn xóa ' .$model->name,
                                                         'data-method' =>'post',
                                                        ]);
                    },

            ]

            ],   
        ],
    ]); ?>
        </div>
    </div>


</div>
