<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;  //Truyền dữ liệu này vào để lấy dữ liệu ra.  Khi dùng thì nó sẽ ra cái mảng giống ở dưới chỗ dropDownList
use yii\widgets\ActiveForm;
use backend\models\Category;

/* @var $this yii\web\View */
/* @var $model backend\models\Category */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="category-form">

	<div class="panel panel-primary">
        <div class="panel-body">
            <h3 class="btn btn-primary"><?= Html::encode($this->title) ?></h3>
        </div>
    <div class="panel-body">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>
    
    <!-- Phải có cái này mới hiện dữ liệu kiểu -- ở bên category đc -->
    <?php 
        $cat = new Category;
     ?>

    <?= $form->field($model, 'parent')->dropDownList( //1. Mảng data đầu tiên là lấy từ trong dữ liệu ra
        [
            $cat->getParent(), //Không cần phải cái ArrayHelper ở dưới nữa vì đã gọi hàm Category rồi
             // ArrayHelper::map(Category::find()->all(), 'id', 'name' ),         //Đơn giản thì dùng ArrayHelper....  ArrayHelper::map: Nó map cái danh sách mà lấy được trong Category -> nó trả về mảng giống ở dưới fixx cứng....       Category::find()->all(), 'id':trường id, 'name': trường name ở trong Category

            //Đây là fix cứng để làm VD
            // 1=>'yii framwork',
            // 2=>'yii2 framwork',
            // 3=>'--- php-mvc'
        ],
        [
            'prompt' =>'Danh mục cha'
        ],
    ) ?>

    <?= $form->field($model, 'status')->dropDownList( //Truyền vào 2 tham số: 1. Mảng data   2. Mảng nhưng truyền vào chữ (VD: trạng thái) mình muốn
        [
            1=>'Kích hoạt',
            0=>'Không kích hoạt'
        ],
        [
            'prompt' =>'Chọn trạng thái'
        ],
    ) ?>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    </div>
</div>
</div>
