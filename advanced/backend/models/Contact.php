<?php 
	namespace backend\models;
	use yii\db\ActiveRecord;
	
	/**
	 * summary
	 */
	class Contact extends ActiveRecord
	{
	    public static function tableName()  //cho dữ liệu vào bảng thì phải thêm static vào vì nó là hướng đối tượng
	    {
	     	return 'contact';   
	    }

	    public function rules()
	    {
	    	$rules = [
	    		[['name', 'email', 'subject', 'body'], 'required'],   //['tên các trường trong database', 'validate']
	    		[['name', 'subject'], 'string', 'max' => 255],
	    		['email', 'string', 'max' => 100],
	    		['body', 'safe'],  //safe: nghĩa là cái gì cũng được
	    		['email', 'email'],
	    		[['created_at','updated_at'], 'integer']

	    	];

	    	return $rules;
		 
	    }

	    

	}

 ?>

 