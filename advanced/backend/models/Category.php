<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "category".
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property int $parent
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    private $_cats = [];  //Muốn có dạng -- tên trường thì khai báo thuộc tính ở đây

    public static function tableName()
    {
        return 'category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'slug', 'created_at', 'updated_at'], 'required'],
            [['parent', 'status', 'created_at', 'updated_at'], 'integer'],
            [['name', 'slug'], 'string', 'max' => 255],
            [['name'], 'unique'],
            [['slug'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Tên danh mục',
            'slug' => 'Đường dẫn tĩnh',
            'parent' => 'Danh mục cha',
            'status' => 'Trạng thái',
            'created_at' => 'Ngày tạo',
            'updated_at' => 'Ngày cập nhật',
        ];
    }

    public function getParent()
    {
        //Truyền vào 2 tham số
        $data = Category::find()->all();   //Cái này phải có mới foreach đc.  Còn nhỡ dữ liệu đang mới chưa truyền dữ liệu đc nên phải if trc

        $leval='-- ';
        if($data) :
            foreach ($data as $item) :
                if ($item->parent == 0) { //Nếu parent này == 0 thì cái $leval = rỗng.... Còn lại thì nó sẽ tự nối cái leval này vào sau cái tên
                    $leval = '';
                }
                $this->_cats[$item->id] = $leval.$item->name;        //Cats: Là mảng bao gồm 2 thứ là cái id(ở ngoài ta map cái 'id')

            endforeach;
            
        endif; 

        return $this->_cats;
    }
}
