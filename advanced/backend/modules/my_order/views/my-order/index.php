<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\my_order\models\MyOrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'My Orders');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="my-order-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <!-- </?= Html::button ('Create My Order', ['value' =>Url::to('index.php?r=my_order/create'), 'class' => 'btn btn-success', 'id' =>'modalButton']) ?> -->
        <?= Html::button(Yii::t('app', 'Create My Order'), ['value' => Url::to('index.php?r=my_order/my-order/create'), 'class' => 'btn btn-success', 'id' =>'modalButton']) ?>
    </p>

    <?php 
        Modal::begin([
            'header' =>'<h4>MyOrder</h4>', 
            'id' => 'modal',
            'size' => 'modal-lg',
        ]);

        echo "<div id='modalContent'></div>";

        Modal::end();
        
     ?>

    <?php Pjax::begin(['id' =>'my-orderGrid']); ?>    <!-- Truyền ID cho nó: ID truyền vào là 1 nhánh và lưu ID sao chép trở lại -->
                        <!-- </?php Pjax::begin(['enablePushState' => false]); ?> -->                             <!-- Phân trang thì cần dùng đến cái phần trong ngoặc -->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn',
             'header' => 'STT',
             'headerOptions' => ['style' => 'width:20px; text-align:center'],
             'contentOptions' => ['style' => 'width:20px; text-align:center'],
            ],

            // 'columns' => ['class' => 'yii\grid\CheckBoxColumn'],

            //'id',
            [
                'attribute' => 'id',
                'label' => 'ID',
                'headerOptions' => ['style' => 'width:30px; text-align:center'],
                'contentOptions' => ['style' => 'width:30px; text-align:center'],
            ],  


             //'product_name',
             [
                'attribute' => 'product_name',
                'label' => 'product_name',
                'headerOptions' => ['style' => 'width:180px; text-align:center'],
                'contentOptions' => ['style' => 'width:180px; text-align:center'],
             ], 


            //'price',
            [
                'attribute' => 'price',
                'label' => 'price',
                'headerOptions' => ['style' => 'width:120px; text-align:center'],
                'contentOptions' => ['style' => 'width:120px; text-align:center'],
             ],


            //'user_id',
            [
                'attribute' => 'user_id',
                'label' => 'user_id',
                'headerOptions' => ['style' => 'width:180px; text-align:center'],
                'contentOptions' => ['style' => 'width:180px; text-align:center'],
             ],

            // 'purchase_date',
            // [
            //     'attribute' => 'purchase_date',
            //     'content' => function($model)
            //     {
            //         return date('yyyy-m-d',$model->purchase_date);
            //     }
            //  ],

            // 'purchase_date',
             [
                'attribute' => 'purchase_date',
                'label' => 'purchase_date',
                'headerOptions' => ['style' => 'width:180px; text-align:center'],
                'contentOptions' => ['style' => 'width:180px; text-align:center'],
             ],


            ['class' => 'yii\grid\ActionColumn',
             'header' => 'Thao tác',
             'headerOptions' => ['style' => 'width:250px; text-align:center'],
             'contentOptions' => ['style' => 'width:250px; text-align:center'],

             'buttons' => [
                'view' => function($url, $model)
                {
                    return Html::a('View', $url, ['class' => 'btn btn-sm btn-primary']);
                },

                'update' => function($url, $model)
                {
                    return Html::a('Edit', $url, ['class' => 'btn btn-sm btn-success']);
                },

                'delete' =>function($url, $model)
                {
                    return false;
                }
            ],

            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
