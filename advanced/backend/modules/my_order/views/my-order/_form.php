<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\modules\my_order\models\MyOrder;
use backend\modules\my_order\models\MyItems;
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\modules\my_order\models\MyOrder */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="my-order-form">

    <?php $form = ActiveForm::begin(['id'=>$model->formName()]); ?> <!-- Tập tin mẫu tồn tại -->

    <?= $form->field($model, 'product_name')->textInput(['maxlength' => true]) ?> 

    <!-- </?php 
    $product_name = new MyOrder;
     ?> -->
    <!-- </?= $form->field($model, 'product_name')->dropDownList(
    	// $product_name->getProductName(),
    	ArrayHelper::map(MyItems::find()->all(), 'item_producer', 'item_name'),
    	[
    		'prompt' => 'Danh mục tên sản phẩm '
    	],
    ) ?> -->    

    <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'user_id')->textInput() ?>

    <!-- </?= $form->field($model, 'purchase_date')->textInput() ?> -->
    <?= $form->field($model, 'purchase_date')->widget(
	    DatePicker::className(), [
	        // inline too, not bad
	         'inline' => false, 
	         // modify template for custom rendering
	        //'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
	        'clientOptions' => [
	            'autoclose' => true,
	            'format' => 'yyyy-m-d'
	        ]
		]);?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php 
$script = <<< JS

$('form#{$model->formName()}').on('beforeSubmit', function(e)  
{
	var \$form = $(this);
	    $.post(   /*gửi dữ liệu bằng cách sử dụng một yêu cầu của bài viết*/
			\$form.attr("action"),   /*serialize Yii2 form*/
			\$form.serialize()
		)
			.done(function(result){
			//console.log(result); //Bảng điều khiển trang web để làm mới trang
			if(result == 1)  //Kiểm tra nếu có trong csdl bên items = 1 thì in ra. Còn nếu ko = 1 thì ko in
			{
				$(document).find('#modal').modal('hide');   //Ẩn đi vì đã có id mẫu bên index
				$(\$form).trigger("reset");   //Nếu được kích hoạt thì loại bỏ 
				$.pjax.reload({container:'#my-orderGrid'}); //Truyền vào # cái id mà bên index đã gọi đến
			}else
			{
				// $(\$form).trigger("reset");
				$("#message").html(result);
			}
			}).fail(function(){
				console.log("server error");
			});

			return false;

	});
JS;
$this->registerJs($script);
 ?>


