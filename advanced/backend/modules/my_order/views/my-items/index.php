<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\my_order\models\MyItemsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'My Items');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="my-items-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <!-- </?= Html::a(Yii::t('app', 'Create My Items'), ['create'], ['class' => 'btn btn-success']) ?> -->
        <?= Html::button(Yii::t('app', 'Create My Items'), ['value' =>Url::to('index.php?r=my_order/my-items/create'),  'class' => 'btn btn-success', 'id' => 'modalButton']) ?>
    </p>

    <?php 
        Modal::begin([
            'header' => '<h4>MyItems</h4>',
            'id' => 'modal',
            'size' => 'modal-lg'
        ]);

        echo "<div id='modalContent'></div>";

        Modal::end()
     ?>

    <?php Pjax::begin(['id'=>'my-itemsgrid']); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn',
             'header' => 'STT',
             // 'label' => 'STT',
             'headerOptions' => ['style' => 'width:20px; text-align:center'],
             'contentOptions' => ['style' => 'width:20px; text-align:center'],
            ],

            // ['class' => 'yii\grid\CheckBoxColumn',
            //  'headerOptions' => ['style' => 'width:50px; text-align:center;' ],
            //  'contentOptions' => ['style' => 'width:50px; text-align:center;' ],
            // ],

            //'item_name',
            [
                'attribute' =>'item_name',
                'label' => 'item_name',
                'headerOptions' => ['style' => 'width:200px; text-align:center;' ],
                'contentOptions' => ['style' => 'width:200px; text-align:center;' ],
            ],


            // 'item_producer',
            [
                'attribute' =>'item_producer',
                'header' => 'item_producer',
                'headerOptions' => ['style' => 'width:180px; text-align:right;  font-weight: bold' ],
                'contentOptions' => ['style' => 'width:180px; text-align:right; font-weight: bold' ],
            ],

            //'item_expiry_date',
            [
                'attribute' =>'item_expiry_date',
                'label' => 'item_expiry_date',
                'headerOptions' => ['style' => 'width:200px; text-align:center;' ],
                'contentOptions' => ['style' => 'width:200px; text-align:center;' ],
            ],


            //Thao tác
            ['class' => 'yii\grid\ActionColumn',
             'header' => 'Thao tác',
             'headerOptions' => ['style' => 'width:250px; text-align:center;' ],
             'contentOptions' => ['style' => 'width:250px; text-align:center;' ],

                'buttons' => [
                    'view' => function($url, $model)
                    {
                        return Html::a('View', $url, ['class' => 'btn btn-sm btn-primary']);

                    },

                    'update' => function($url, $model)
                    {
                        return Html::a('Edit', $url, ['class' => 'btn btn-sm btn-success']);
                    },

                    'delete' => function($url, $model)
                    {
                        return false;
                    },
                             ],
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
