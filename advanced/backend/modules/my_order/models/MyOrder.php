<?php

namespace app\modules\my_order\models;

use Yii;

/**
 * This is the model class for table "{{%my_order}}".
 *
 * @property int $id
 * @property string $product_name
 * @property string $price
 * @property int $user_id
 * @property string $purchase_date
 *
 * @property Items $productName
 */
class MyOrder extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%my_order}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_name', 'user_id', 'purchase_date'], 'required'],
            [['price'], 'number'],
            [['user_id'], 'integer'],
            [['purchase_date'], 'safe'],
            [['product_name'], 'string', 'max' => 255],
            [['product_name'], 'exist', 'skipOnError' => true, 'targetClass' => MyItems::className(), 'targetAttribute' => ['product_name' => 'item_name']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'product_name' => Yii::t('app', 'product_name'),
            'price' => Yii::t('app', 'price'),
            'user_id' => Yii::t('app', 'user_id'),
            'purchase_date' => Yii::t('app', 'purchase_date'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductName()
    {
        return $this->hasOne(MyItems::className(), ['item_name' => 'product_name']);
    }

}
