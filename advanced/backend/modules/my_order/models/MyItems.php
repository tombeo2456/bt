<?php

namespace app\modules\my_order\models;

use Yii;

/**
 * This is the model class for table "{{%items}}".
 *
 * @property string $item_name
 * @property string $item_producer
 * @property string $item_expiry_date
 */
class MyItems extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%items}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['item_name', 'item_producer', 'item_expiry_date'], 'required'],
            [['item_expiry_date'], 'safe'],
            [['item_name', 'item_producer'], 'string', 'max' => 255],
            [['item_name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'item_name' => Yii::t('app', 'Item Name'),
            'item_producer' => Yii::t('app', 'Item Producer'),
            'item_expiry_date' => Yii::t('app', 'Item Expiry Date'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMyOrders()
    {
        return $this->hasMany(MyOrder::className(), ['product_name' => 'item_name']);
    }
}
