<?php

namespace app\modules\my_order\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\my_order\models\MyItems;

/**
 * MyItemsSearch represents the model behind the search form of `app\modules\my_order\models\MyItems`.
 */
class MyItemsSearch extends MyItems
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['item_name', 'item_producer', 'item_expiry_date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MyItems::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'item_expiry_date' => $this->item_expiry_date,
        ]);

        $query->andFilterWhere(['like', 'item_name', $this->item_name])
            ->andFilterWhere(['like', 'item_producer', $this->item_producer]);

        return $dataProvider;
    }
}
