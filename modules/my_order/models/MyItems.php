<?php

namespace app\modules\my_order\models;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "{{%items}}".
 *
 * @property string $item_name
 * @property string $item_producer
 * @property string $item_expiry_date
 */
class MyItems extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%items}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['item_name', 'item_producer', 'item_expiry_date'], 'required'],
            [['item_expiry_date'], 'safe'],
            [['item_name', 'item_producer'], 'string', 'max' => 255],
            [['item_name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'item_name' => Yii::t('app', 'Item Name'),
            'item_producer' => Yii::t('app', 'Item Producer'),
            'item_expiry_date' => Yii::t('app', 'Item Expiry Date'),
        ];
    }

    // public static function model($className=__CLASS__)
    // {
    //     return parent::model($className);        
    // }

    // public function beforeValidate()
    // {
    //     // echo 'beforeValidate';
    //     return parent::beforeValidate();
    // }

    // public function afterValidate()
    // {
    //     // echo 'afterValidate';
    //     return parent::afterValidate();
    // }

    // public function beforeSave($insert)
    // {
    // if (!parent::beforeSave($insert)) {
    //     return false;
    // }

    // // ...custom code here...
    // return true;
    // }

    // public function afterSave($insert, $changedAttributes)
    // {
    // if (!parent::beforeSave($insert, $changedAttributes)) {
    //     return false;
    // }

    // // ...custom code here...
    // return true;
    // }

    // public function getAllMyItems()
    // {
    //     //Trỏ lấy tất cả
    //     $data = MyItems::find()->asArray()->all();   //Trỏ đến asArray thì nó sẽ trả KQ về dạng mảng 2 chiều 
    //     return $data;

    // //Trỏ lấy theo cái mình cần lấy where(..)
    // //     $data = MyItems::find()->where(['item_name' => 'Đạt'])->asArray()->all();   //Trỏ đến asArray thì nó sẽ trả KQ về dạng mảng 2 chiều 
    // //     return $data;

    //  }

}