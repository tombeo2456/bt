<?php

namespace app\modules\my_order\models;

use Yii;
use yii\db\Query;
use yii\db\BaseActiveRecord;

/**
 * This is the model class for table "{{%my_order}}".
 *
 * @property int $id
 * @property string $product_name
 * @property string $price
 * @property int $user_id
 * @property string $purchase_date
 *
 * @property Items $productName
 */
class MyOrder extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%my_order}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_name', 'user_id', 'purchase_date'], 'required', 'message' => '{attribute} không được để trống'],
            [['price'], 'number'],
            [['user_id'], 'integer'],
            [['purchase_date'], 'safe'],
            [['product_name'], 'string', 'max' => 255],
            [['product_name'], 'exist', 'skipOnError' => true, 'targetClass' => MyItems::className(), 'targetAttribute' => ['product_name' => 'item_name']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'product_name' => Yii::t('app', 'product_name'),
            'price' => Yii::t('app', 'price'),
            'user_id' => Yii::t('app', 'user_id'),
            'purchase_date' => Yii::t('app', 'purchase_date'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductName()
    {
        return $this->hasOne(MyItems::className(), ['item_name' => 'product_name']);
    }

    // public static function model($className=__CLASS__)
    // {
    //     return parent::model($className);        
    // }

    // public function beforeValidate()
    // {
    //     // echo 'beforeValidate';
    //     return parent::beforeValidate();
    // }

    // public function afterValidate()
    // {
    //     // echo 'afterValidate';
    //     return parent::afterValidate();
    // }

    // public function beforeSave($insert)
    // {
    // if (!parent::beforeSave($insert)) {
    //     return false;
    // }

    // // ...custom code here...
    // return true;
    // }

    // public function afterSave($insert, $changedAttributes)
    // {
    // if (!parent::beforeSave($insert, $changedAttributes)) {
    //     return false;
    // }

    // // ...custom code here...
    // return true;
    // }

    public function beforeSave($insert)
    {
        if(!parent::beforeSave($insert))
        {
            return false;
        }
            
        if($insert)
            {
            $this->price *= 0.1;
            $this->user_id += 1;
            // $this->product_name .='KH'  ;   
            }        

            return true;
    }

    public function beforeDelete()
    {
       parent::beforeDelete();

       return true;
    }


       public function afterSave($insert, $changedAttributes)
        {
        $this->trigger($insert ? self::EVENT_AFTER_INSERT : self::EVENT_AFTER_UPDATE, new AfterSaveEvent([
            'changedAttributes' => $changedAttributes,
        ]));
        }

    public function afterDelete()
    {
        $this->trigger($insert ? self::EVENT_AFTER_INSERT : self::EVENT_AFTER_UPDATE, new AfterSaveEvent([
            'changedAttributes' => $changedAttributes,
        ]));
    }

}
