﻿<?php

namespace app\modules\my_order\controllers;

use Yii;
use app\modules\my_order\models\MyOrder;
use app\modules\my_order\models\MyOrderSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\my_order\models\MyItems;
use app\modules\my_order\models\MyItemsSearch;
use yii\web\Response\FORMAT_JSON;
use yii\helpers\ArrayHelper;

/**
 * MyOrderController implements the CRUD actions for MyOrder model.
 */
class MyOrderController extends Controller
{
    /**
     * ssssssss, sssssss
     * dddd, dddd
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MyOrder models.
     * @return mixed
     */
    public function actionIndex()
    {
        // $events = MyOrder::find()->all();

        // foreach ($events as $event) {
        //   $event = new \yii2fullcalendar\models\Event();
        //   $event->id = $event->id;
        //   $event->product_name = $event->product_name;
        //   $event->price = $event->price;
        //   $event->user_id = $event->user_id;
        //   $event->start = date('Y-m-d\Th:m:s\Z',strtotime('tomorrow 6am'));
        //   $events[] = $event;
        // }

        //làm theo pjax
        $model = new MyOrder();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model = new MyOrder();
        }

        // echo "Vào hàm validate";
        // if (!$model->validate()) 
        // echo '<pre>';
        // var_dump($model->getErrors());
        // echo '</pre>';  

        // echo '<pre>';
        // var_dump($model->getErrors());
        // echo '</pre>';
        
        //    echo "Hết validate <br>"; 
        
        // echo 'Lưu hàm: <br>';
        // if (!$model->save())
        //     echo '<pre>';
        //     var_dump($model->getErrors());
        //     echo '</pre>';
        //     ; 
        //    echo "save"; 

        $searchModel = new MyOrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            // 'events' => $events,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model,
        ]);
    }

    /**
     * Displays a single MyOrder model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MyOrder model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MyOrder();
        // $myitems = new MyItems();
        // $dataMyItems = ArrayHelper::map($myitems->getAllMyItems(), "item_name","item_name");
        
       // echo '<pre>';
       // var_dump($dataMyItems);
       // echo '</pre>';
       // die();

        if ($model->load(Yii::$app->request->post())) {
            // return $this->redirect(['view', 'id' => $model->id]);
            
            if ($model->save()) 
            {
                 return $this->redirect(['view', 'id' => $model->id]);
            }
            else 
            {
              echo 0;  
            }

            // $model->id=$this->beforeSave();
            // $model->save();
            // return $this->redirect(['view', 'product_name' => $model->product_name]);

        } else
        { return $this->renderAjax('create', [
            'model' => $model,
            // 'dataMyItems' => $dataMyItems,
        ]);
    }
    }

    /**
     * Updates an existing MyOrder model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        else
        { 
            return $this->render('update', [
            'model' => $model,
        ]);
        }
    }

    /**
     * Deletes an existing MyOrder model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        if (!Yii::$app->request->isAjax) {
            return $this->redirect(['index']);
        }

        
    }

    /**
     * Finds the MyOrder model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MyOrder the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MyOrder::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionFormSubmission()
    {
        $security = new Security();
        $string = Yii::$app->request->post('string');
        $stringHash = '';
        if(!is_null($string)){
            $stringHash = $security->generatePasswordHash($string);
        }
        return $this->render('form-submission', [
            'stringHash' => $stringHash,
        ]);
    }

    
}
