<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\widegts\Pjax;
use app\modules\my_order\models\MyOrder;
use app\modules\my_order\models\MyItems;   //Namespace bên Items truyền thế nào thì use bên này truyền như thế
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\modules\my_order\models\MyOrder */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="my-order-form">

	<?php yii\widgets\Pjax::begin(['id' => 'my_order']) ?>
    <!-- </?php $form = ActiveForm::begin(['id' => 'new_my-order']); ?> --> <!-- Tập tin mẫu tồn tại -->
    <?php $form = ActiveForm::begin(['options' => ['data-pjax' => true ]]); ?>

	<!-- Lấy dữ liệu thông thường -->
    <?= $form->field($model, 'product_name')->dropDownList(
    	ArrayHelper::map(MyItems::find()->all(), 'item_name', 'item_name'),
    	[
    		'prompt' => 'Danh mục tên sản phẩm '
    	],
    ) ?> 

	<!-- Lấy dữ liệu kiểu Truy Vấn CSDL -->
    <!-- </?= $form->field($model, 'product_name')->dropDownList($dataMyItems,
    	[
    		'prompt' => 'Danh mục tên sản phẩm- '
    	],
    ) ?>   -->

    <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'user_id')->textInput() ?>

    <!-- </?= $form->field($model, 'purchase_date')->textInput() ?> -->
    <?= $form->field($model, 'purchase_date')->widget(
	    DatePicker::className(), [
	        // inline too, not bad
	         'inline' => false, 
	         // modify template for custom rendering
	        //'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
	        'clientOptions' => [
	            'autoclose' => true,
	            'format' => 'yyyy-m-d'
	        ]
		]);?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Thêm mới'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
	<?php yii\widgets\Pjax::end() ?>
</div>

<!-- </?php 
		$this->registerJs('
			$("document").ready(function(){
				$("#my-order").on("pjax:end", function(){
					$.pjax.reload({container:"#myorderGrid});
					});
			});
		');
	 ?> -->

<?php 
  $this->registerJs('
    $("document").ready(function(){
      $("#my_order").on("pjax:end", function(){
        $.pjax.reload({container:"#myorderGrid"})

        });
      });
    ');
 ?>




