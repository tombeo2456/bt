<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\my_order\models\MyItems */

$this->title = Yii::t('app', 'Update My Items: {name}', [
    'name' => $model->item_name,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'My Items'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->item_name, 'url' => ['view', 'id' => $model->item_name]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="my-items-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
