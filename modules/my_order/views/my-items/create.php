<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\my_order\models\MyItems */

$this->title = Yii::t('app', 'Create My Items');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'My Items'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="my-items-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        // 'dataMyItems' => $dataMyItems,
        
    ]) ?>

</div>
