-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 21, 2019 at 03:52 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `yiidemo`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parent` int(11) NOT NULL DEFAULT '0',
  `status` smallint(6) NOT NULL DEFAULT '0',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `slug`, `parent`, `status`, `created_at`, `updated_at`) VALUES
(1, 'yii framwork', 'yii-framwork', 0, 1, 1555054165, 1555054165),
(2, 'yii2 framwork', 'yii2-framwork', 0, 0, 1555054173, 1555054173),
(3, 'php-mvc', 'php-MVC', 2, 1, 1555054180, 1555054180);

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `parent` int(11) NOT NULL DEFAULT '0',
  `status` smallint(6) NOT NULL DEFAULT '0',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `name`, `email`, `subject`, `body`, `parent`, `status`, `created_at`, `updated_at`) VALUES
(1, 'vansidat', 'tombeo2456@gmail.com', 'Demo test', '1234', 0, 0, 1554973435, 1554973435);

-- --------------------------------------------------------

--
-- Table structure for table `democate`
--

CREATE TABLE `democate` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parent` int(11) NOT NULL DEFAULT '0',
  `status` smallint(6) NOT NULL DEFAULT '0',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `democate`
--

INSERT INTO `democate` (`id`, `name`, `slug`, `parent`, `status`, `created_at`, `updated_at`) VALUES
(1, 'demo', 'he', 0, 0, 1555231717, 1555231717),
(2, 'haha', '1', 1, 1, 1555231726, 1555231726),
(3, 'demo3', 'he3', 2, 1, 1555231730, 1555231730);

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `item_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item_producer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item_expiry_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`item_name`, `item_producer`, `item_expiry_date`) VALUES
('abc1', '2', '2019-04-29'),
('Đạt', 'Cty LinxHQ', '2019-04-20'),
('Đạt1', '1', '2019-04-23'),
('Đạt2', '2', '2019-04-16'),
('Đạt3', '11111', '2019-04-30');

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1554913621),
('m130524_201442_init', 1554976624),
('m190124_110200_add_verification_token_column_to_user_table', 1554976624),
('m190410_163258_category', 1554914501),
('m190411_074955_contact', 1554969683),
('m190412_094413_democate', 1555062500),
('m190417_143431_my_items', 1555511693),
('m190417_143809_my_order', 1555511955);

-- --------------------------------------------------------

--
-- Table structure for table `my_order`
--

CREATE TABLE `my_order` (
  `id` int(11) NOT NULL,
  `product_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` decimal(15,2) NOT NULL DEFAULT '0.00',
  `user_id` int(11) NOT NULL,
  `purchase_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `my_order`
--

INSERT INTO `my_order` (`id`, `product_name`, `price`, `user_id`, `purchase_date`) VALUES
(84, 'Đạt', '1.00', 2, '2019-04-23'),
(85, 'Đạt', '22.00', 22, '2019-04-07'),
(86, 'Đạt3', '100000.00', 1, '2019-04-29');

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE `post` (
  `item_name` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `verification_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`, `verification_token`) VALUES
(1, 'datbeo', '2Jl75McGSmROdPDV8Z2mHueirI2Tb9dZ', '$2y$13$tlZ75DG.uQV.8WEt6lIpNOjmSnG7Ry7cWtSrLIWjB15t2v5TNvZKa', NULL, 'tombeo2456@gmail.com', 9, 1554976633, 1554976633, 'qiERFAdiRa-0ppbEU1eW1O6dDyFlmE3Q_1554976633'),
(2, 'datdat', 'qdGrNxMdfQzwZtnUSQHf7nlI9AAHNRlc', '$2y$13$RDn04hW5MXkahPDgZWI62.Q1yoRZ7JeoXylBSEa/4CQg7bUsy8guu', NULL, 'dad@gmail.com', 9, 1555240487, 1555240487, 'usXcTyMrBxJYwZLOLHOF3Iv9M7SrkdSo_1555240487'),
(3, 'datdd', 'oeeOuFMq47P0f2MGeYr0CQSo1j6kKPO_', '$2y$13$rCOhO5mrAzMlguKHdk0f5.HDZ3orqXaOAAyUBUN7C3XvU13MxSikq', NULL, '123456@gmail.com', 9, 1555322677, 1555322677, 'ihfL-XaZA7w9DKwIeV5TZ33RulebrjCc_1555322677'),
(4, 'datbeo1', '4t2bTsOiLmBRgzHf2cQzzXOsi_l4sJfE', '$2y$13$Nm71WpT.bN6Q/zy7sydKXe7okpPFy7UkrZCMp2uSBO225yuj78pea', NULL, 'aaa11@gmail.com', 9, 1555331619, 1555331619, 'ljLUTWpRH7Uv_adJ_LbGmjMB9qs7yqhS_1555331619'),
(5, 'datbeo2', 'oCg-asSg1LzjSalAsblLBz_i1QIBGtfX', '$2y$13$ZmfFGHTiRxTzCyakb40AluteF4Ahp7o5oBLm3f.VJc6mpgBu3peLO', NULL, 'da23d@gmail.com', 9, 1555331730, 1555331730, 'vuW3blYE8J_J_Yseg2Kp92veYnRj8Vxx_1555331730'),
(6, 'vsdfat', 'KnJ9CnWJZ6Nzf94E-YqWcipSZHNUgWHG', '$2y$13$bGwD1Ch3sCQtfDz2ZCd4weR9wOc/QOfv4zj6mYpn9phX4auTBIig.', NULL, 'datdat@gmail.com', 9, 1555343631, 1555343631, 'FQLjqpJsJx51QazZaGzjWd2_LGF-zMtC_1555343631');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `democate`
--
ALTER TABLE `democate`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`item_name`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `my_order`
--
ALTER TABLE `my_order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-my_order-product_name` (`product_name`);

--
-- Indexes for table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`item_name`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `democate`
--
ALTER TABLE `democate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `my_order`
--
ALTER TABLE `my_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=87;

--
-- AUTO_INCREMENT for table `post`
--
ALTER TABLE `post`
  MODIFY `item_name` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `my_order`
--
ALTER TABLE `my_order`
  ADD CONSTRAINT `fk-post-product_name` FOREIGN KEY (`product_name`) REFERENCES `items` (`item_name`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
